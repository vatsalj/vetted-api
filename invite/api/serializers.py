from rest_framework import serializers

from invite import models


class AcceptInviteSerializer(serializers.Serializer):
    key = serializers.UUIDField()
    password = serializers.CharField()  # TODO: Use settings to take fields for post-acceptance form
    confirm_password = serializers.CharField()

    def validate(self, attrs):
        key, password, confirm_password = attrs.get('key'), attrs.get('password'), attrs.get('confirm_password')
        try:
            invite = models.Invite.pending_invites.get(key=key)
        except models.Invite.DoesNotExist:
            raise serializers.ValidationError({'key': "This key is not valid"})

        if password != confirm_password:
            raise serializers.ValidationError({'confirm_password': "Passwords do not match"})
        attrs['invite'] = invite
        return attrs


class RejectInviteSerializer(serializers.Serializer):
    key = serializers.UUIDField()

    def validate(self, attrs):
        key = attrs.get('key')
        try:
            invite = models.Invite.pending_invites.get(key=key)
        except models.Invite.DoesNotExist:
            raise serializers.ValidationError({'key': "This key is not valid"})
        attrs['invite'] = invite
        return attrs


class InviteKeySerializer(serializers.Serializer):
    key = serializers.UUIDField(error_messages={
        'invalid': "This key is not valid"
    })

    def validate(self, attrs):
        key = attrs.get('key')
        try:
            invite = models.Invite.pending_invites.get(key=key)
        except models.Invite.DoesNotExist:
            raise serializers.ValidationError({'key': "This key is not valid"})
        attrs['invite'] = invite
        return attrs
