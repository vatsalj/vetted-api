from rest_framework.viewsets import ModelViewSet
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import AllowAny
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from invite.models import Invite
from invite.api import serializers
from invite import signals
from utils.rest_framework.permissions import IsSuperUser


class InviteViewSet(ModelViewSet):
    permission_classes = [IsSuperUser]   # TODO: use settings instead of hardcoded permissions
    authentication_classes = [TokenAuthentication]  # TODO: use settings instead
    queryset = Invite.objects.all()
    serializer_class = []

    @action(methods=['post'], detail=False, url_path='accept', permission_classes=[AllowAny])
    def accept_invite(self, request):
        data = request.data.copy()
        invite_action = Invite.ACCEPTED
        data['action'] = invite_action
        serializer = serializers.AcceptInviteSerializer(data=data)
        if serializer.is_valid():
            invite = serializer.validated_data['invite']
            invite.status = invite_action
            invite.save()

            signals.invitation_accepted.send(
                sender=self.__class__,
                user=invite.invitee,
                password=serializer.validated_data['password'],
            )
            return Response({})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['post'], detail=False, url_path='reject', permission_classes=[AllowAny])
    def reject_invite(self, request):
        data = request.data.copy()
        invite_action = Invite.REJECTED
        data['action'] = invite_action
        serializer = serializers.RejectInviteSerializer(data=data)
        if serializer.is_valid():
            invite = serializer.validated_data['invite']
            invite.status = invite_action
            invite.save()

            signals.invitation_rejected.send(
                sender=self.__class__,
                user=invite.invitee,
            )
            return Response({})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['post'], detail=False, url_path='validate-key', permission_classes=[AllowAny])
    def validate_key(self, request):
        serializer = serializers.InviteKeySerializer(data=request.data)
        if serializer.is_valid():
            return Response({}, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
