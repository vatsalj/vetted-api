from model_mommy import mommy
from django.conf import settings

from model_mommy.recipe import Recipe
from invite.models import Invite


pending_invite = Recipe(
    Invite,
    status=Invite.PENDING,
)


rejected_invite = Recipe(
    Invite,
    status=Invite.REJECTED,
)


accepted_invite = Recipe(
    Invite,
    status=Invite.ACCEPTED,
)
