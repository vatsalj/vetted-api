# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid

from django.conf import settings
from django.db import models
from django.db.models import manager


class PendingInviteManager(manager.Manager):
    def get_queryset(self, *args, **kwargs):
        queryset = super(PendingInviteManager, self).get_queryset(*args, **kwargs)
        return queryset.filter(status=Invite.PENDING)


class Invite(models.Model):
    ACCEPTED = 0
    PENDING = 1
    REJECTED = 2
    STATUS_CHOICES = (
        (ACCEPTED, "Accepted"),
        (PENDING, "Pending"),
        (REJECTED, "Rejected")
    )

    ALREADY_ACTIVATED_VALUE = 'ALREADY_ACTIVATED'

    invitee = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='invite')
    invitor = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='invited_users')
    key = models.UUIDField(default=uuid.uuid1)
    status = models.PositiveSmallIntegerField()
    expires_at = models.DateTimeField(null=True, blank=True)  # TODO

    objects = manager.Manager()
    pending_invites = PendingInviteManager()

    @classmethod
    def create_invite(cls, invitee, invitor):
        return cls.objects.create(
            invitee=invitee,
            invitor=invitor,
            key=cls.create_activation_key(),
            status=cls.PENDING
        )

    @classmethod
    def create_activation_key(cls):
        key = uuid.uuid4()
        while cls.objects.filter(key=key).exists():
            key = uuid.uuid4()
        return key
