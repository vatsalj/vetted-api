# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class InvitationsConfig(AppConfig):
    name = 'invite'

    def ready(self):
        import invite.signals.handlers  # noqa
