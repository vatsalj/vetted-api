from celery import shared_task
from django.conf import settings
from django.core.mail import EmailMessage
from django.shortcuts import get_object_or_404

from invite import models


@shared_task()
def send_invite(invite_id_or_obj):
    if isinstance(invite_id_or_obj, int):
        invite = get_object_or_404(models.Invite, id=invite_id_or_obj)
    else:
        invite = invite_id_or_obj

    email = EmailMessage(
        "Activate your account",
        # TODO: coupled org logic into invites here which can be circumvented
        "Click on http://{}.vetted.angrynerd.in/invite/view/{} to activate your account".format(
            invite.invitee.org_user_profile.organization.subdomain,
            invite.key
        ),
        # TODO: use settings for front-end URL
        settings.DEFAULT_FROM_EMAIL,
        [invite.invitee.email, ],
        reply_to=[invite.invitor.email, ],
    )
    return email.send(fail_silently=False)
