from django.dispatch import receiver
from django.conf import settings

from invite.models import Invite
from invite.tasks import send_invite
from invite.exceptions import IncorrectlyConfiguredException
import importlib

try:
    invite_user_on_signal_str = settings.INVITE_USER_ON_SIGNAL
except AttributeError:
    raise IncorrectlyConfiguredException("INVITE_USER_ON_SIGNAL not present in settings")

try:
    module_name, signal_name = invite_user_on_signal_str.rsplit('.', 1)
except AttributeError:
    raise IncorrectlyConfiguredException("INVITE_USER_ON_SIGNAL should be a string")

try:
    module = importlib.import_module(module_name)
except ImportError:
    raise IncorrectlyConfiguredException("INVITE_USER_ON_SIGNAL: App with name {} not found".format(module_name))

try:
    invite_user_on_signal = getattr(module, signal_name)
except AttributeError:
    raise IncorrectlyConfiguredException("INVITE_USER_ON_SIGNAL: Signal {} cannot be found inside {}".format(
        module_name, signal_name
    ))

if not {'invitee', 'invitor'} == invite_user_on_signal.providing_args:
    raise IncorrectlyConfiguredException(
        "{} should accept invitee & invitor args only".format(invite_user_on_signal_str)
    )


@receiver(invite_user_on_signal)
def invite_user(sender, invitee, invitor, **kwargs):
    invite = Invite.create_invite(
        invitee=invitee,
        invitor=invitor
    )

    if getattr(settings, 'CELERY_ENABLED', False):
        # If celery is working in the environment, send the invite ID as some brokers have caps on message size
        send_invite.delay(invite.id)
    else:
        # If celery is not working, save the extra DB query by sending the object directly
        send_invite(invite)
