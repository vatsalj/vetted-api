from django.dispatch import Signal


invitation_accepted = Signal(providing_args=['email', 'password', 'hostname', ])

invitation_rejected = Signal(providing_args=['email', 'hostname', ])
