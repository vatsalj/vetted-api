from rest_framework.routers import DefaultRouter

from invite.api.viewsets import InviteViewSet

router = DefaultRouter()
router.register(r'api/v1/invite', InviteViewSet, basename='invite')

urlpatterns = router.urls
