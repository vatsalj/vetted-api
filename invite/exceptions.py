class IncorrectlyConfiguredException(Exception):
    def __init__(self, message="Invite app is not configured properly"):
        self.message = message

    def __str__(self):
        return self.message
