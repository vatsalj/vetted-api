from rest_framework.test import APIClient
from rest_framework import status

from model_mommy import mommy
import uuid
from django.test import TestCase
from mock import patch


class InviteViewSetTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_validate_valid_invite_key(self):
        pending_invite = mommy.make_recipe('invite.pending_invite')
        data = {
            'key': pending_invite.key
        }
        response = self.client.post('/api/v1/invite/validate-key/', data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_validate_invalid_invite_key(self):
        data = {
            'key': uuid.uuid4()
        }
        response = self.client.post('/api/v1/invite/validate-key/', data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_success_on_pending_invite_acceptance_matching_passwords(self):
        pending_invite = mommy.make_recipe('invite.pending_invite')
        data = {
            'key': pending_invite.key,
            'password': 'pass',
            'confirm_password': 'pass'
        }
        response = self.client.post('/api/v1/invite/accept/', data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_success_on_pending_invite_acceptance_not_matching_passwords(self):
        pending_invite = mommy.make_recipe('invite.pending_invite')
        data = {
            'key': pending_invite.key,
            'password': 'pass',
            'confirm_password': 'pass1'
        }
        response = self.client.post('/api/v1/invite/accept/', data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_failure_on_acted_invite_acceptance(self):
        accepted_invite = mommy.make_recipe('invite.accepted_invite')
        data = {'key': accepted_invite.key}
        response = self.client.post('/api/v1/invite/accept/', data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_failure_on_invalid_invite_key_action(self):
        data = {
            'key': uuid.uuid4(),
            'password': 'pass',
            'confirm_password': 'pass'
        }
        response = self.client.post('/api/v1/invite/accept/', data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_success_on_pending_invite_rejection(self):
        pending_invite = mommy.make_recipe('invite.pending_invite')
        data = {
            'key': pending_invite.key
        }
        response = self.client.post('/api/v1/invite/reject/', data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_failure_on_acted_invite_rejection(self):
        accepted_invite = mommy.make_recipe('invite.accepted_invite')
        data = {
            'key': accepted_invite.key
        }
        response = self.client.post('/api/v1/invite/reject/', data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
