# vetted-api

## Project setup (development)
1. Install pipenv
2. Clone this project
3. `pipenv install --dev`
4. Copy conf.ini.sample to conf.ini & make changes as required

## Structure
- organization: app containing users, org user profiles, multi-tenancy via subdomains
- invite: user invitations, activations

## TODO
1. Break settings into individual modules for different environment
2. Use .env rather than conf.ini - it's redundant & old
3. Decouple invite & organization
4. Test cases & more coverage
5. Use docker compose
6. Move user out of organization & into separate app
7. Move to JWT authentication
