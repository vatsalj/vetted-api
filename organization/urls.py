from rest_framework.routers import DefaultRouter

from organization.api import viewsets as org_viewsets


router = DefaultRouter()
router.register(r'api/v1/organization', org_viewsets.OrganizationViewSet, basename='organization')
router.register(r'api/v1/user', org_viewsets.UserViewSet, basename='user')

urlpatterns = router.urls
