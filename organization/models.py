# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models, transaction
from django.db.models import manager
from django.utils.encoding import python_2_unicode_compatible

from organization.exceptions import OrganizationKwargMissingError


class BaseOrganizationModel(models.Model):
    """
    An organization base class to enforce multi-tenancy across the system
    """
    organization = models.ForeignKey("Organization", on_delete=models.CASCADE)

    class Meta:
        abstract = True


class OrganizationUserManager(manager.Manager):
    @staticmethod
    def check_kwarg_or_raise(kwarg, kwarg_str):
        if not kwarg:
            raise OrganizationKwargMissingError("{} kwarg missing in org_users manager".format(kwarg_str))

    def get_queryset(self, *args, **kwargs):
        organization = kwargs.pop('organization', None)
        queryset = super(OrganizationUserManager, self).get_queryset(*args, **kwargs)
        if organization:
            return queryset.filter(org_user_profile__organization=organization)
        return queryset.filter(user_type=User.ORGANIZATION_USER)

    def filter(self, organization=None, *args, **kwargs):
        self.check_kwarg_or_raise(organization, "organization")
        return self.get_queryset(organization=organization).filter(*args, **kwargs)  # TODO check super here

    def get(self, organization=None, *args, **kwargs):
        self.check_kwarg_or_raise(organization, "organization")
        return self.get_queryset(organization=organization).get(*args, **kwargs)  # TODO check super here

    def create(self, org_user_profile=None, organization=None, *args, **kwargs):
        """
        Creates an organization user, along with it's org user profile **atomically**
        """
        self.check_kwarg_or_raise(organization, "organization")
        self.check_kwarg_or_raise(org_user_profile, "org_user_profile")
        kwargs['username'] = User.get_org_username(kwargs['email'], organization.id)
        kwargs['user_type'] = User.ORGANIZATION_USER
        with transaction.atomic():
            org_user = super(OrganizationUserManager, self).create(*args, **kwargs)
            OrganizationUserProfile.objects.create(
                user=org_user,
                organization=organization,
                **org_user_profile
            )
            return org_user

    def create_inactive_user(self, org_user_profile=None, organization=None, *args, **kwargs):
        self.check_kwarg_or_raise(organization, "organization")
        self.check_kwarg_or_raise(org_user_profile, "org_user_profile")
        with transaction.atomic():
            kwargs['is_active'] = False
            org_user = super(OrganizationUserManager, self).create(*args, **kwargs)
            OrganizationUserProfile.objects.create(
                user=org_user,
                organization=organization,
                **org_user_profile
            )
            return org_user


class AdminUserManager(manager.Manager):
    def get_queryset(self, *args, **kwargs):
        super(AdminUserManager, self).get_queryset(*args, **kwargs).filter(user_type=User.ADMIN_USER)


@python_2_unicode_compatible
class User(AbstractUser):
    """
    A user profile model for storing data of organization's user.
    If required, the organization user can be separated for maintaining
    separate schemas

    An :model:organization.Organization's user will be associated with one
    """
    ADMIN_USER = 0
    ORGANIZATION_USER = 1
    USER_TYPE_CHOICES = (
        (ADMIN_USER, "Admin"),
        (ORGANIZATION_USER, "Organization"),
    )
    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    objects = UserManager()
    org_users = OrganizationUserManager()
    admin_users = AdminUserManager()

    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.username

    @classmethod
    def get_org_username(cls, email, organization_id):
        """
        Helper method - since this is a multi-tenant system & username are unique in database,
        this method is a single place to find a workaround by adding organizationID before email
        TODO: some logic for this can be kept in a manager
        :param email:
        :param organization_id:
        :return: Username is a pre-decided format
        """
        return "{}_{}".format(organization_id, email)

    def save(self, *args, **kwargs):
        cls = self.__class__
        if self.id and self.user_type == cls.ORGANIZATION_USER:
            self.username = cls.get_org_username(self.email, self.org_user_profile.organization.id)
        super(User, self).save(*args, **kwargs)


@python_2_unicode_compatible
class OrganizationUserProfile(models.Model):
    ADMIN_ROLE = 0
    STANDARD_ROLE = 1
    ROLE_CHOICES = (
        (ADMIN_ROLE, "Admin"),
        (STANDARD_ROLE, "Standard")
    )

    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='org_user_profile')
    organization = models.ForeignKey("Organization", related_name="users", on_delete=models.CASCADE, editable=False)
    role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    @property
    def is_organization_admin(self):
        return self.role == self.ADMIN_ROLE

    def activate(self, password):
        self.is_active = True
        self.set_password(password)
        self.save()

    def __str__(self):
        return "User: {} Organization: {}".format(self.user.email, self.organization)


@python_2_unicode_compatible
class Organization(models.Model):
    title = models.CharField(max_length=255, unique=True)
    subdomain = models.CharField(max_length=50, unique=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        super(Organization, self).save(*args, **kwargs)

    def __str__(self):
        return self.title
