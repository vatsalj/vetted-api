from django.test import TestCase
from model_mommy import mommy
from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.authtoken.models import Token
from organization import models


class OrganizationUserModelTest(TestCase):

    def setUp(self):
        self.client = APIClient()

        self.org1 = mommy.make_recipe('organization.test_org')
        self.org2 = mommy.make_recipe('organization.test_org')

        self.org1_admin_user = mommy.make_recipe(
            'organization.org_user',
            is_active=True,
            email='a@b.com',
            username=models.User.get_org_username('a@b.com', self.org1.id)
        )
        self.org1_standard_user = mommy.make_recipe('organization.org_user', is_active=True)
        self.org1_standard_user1 = mommy.make_recipe('organization.org_user')

        self.org2_admin_user = mommy.make_recipe('organization.org_user', is_active=True)
        self.org2_standard_user = mommy.make_recipe('organization.org_user')

        self.org1_admin_user_profile = mommy.make_recipe(
            'organization.org_admin_user_profile',
            organization=self.org1,
            user=self.org1_admin_user,
            role=models.OrganizationUserProfile.ADMIN_ROLE,
        )
        self.org1_standard_user_profile = mommy.make_recipe(
            'organization.org_standard_user_profile',
            organization=self.org1,
            user=self.org1_standard_user,
            role=models.OrganizationUserProfile.STANDARD_ROLE,
        )
        self.org1_standard_user1_profile = mommy.make_recipe(
            'organization.org_standard_user_profile',
            organization=self.org1,
            user=self.org1_standard_user1,
            role=models.OrganizationUserProfile.STANDARD_ROLE,
        )
        self.org2_admin_user_profile = mommy.make_recipe(
            'organization.org_admin_user_profile',
            organization=self.org2,
            user=self.org2_admin_user,
            role=models.OrganizationUserProfile.ADMIN_ROLE,
        )
        self.org2_standard_user_profile = mommy.make_recipe(
            'organization.org_standard_user_profile',
            organization=self.org2,
            user=self.org2_standard_user,
            role=models.OrganizationUserProfile.STANDARD_ROLE,
        )

    def test_validate_valid_hostname(self):
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        response = self.client.get("/api/v1/organization/validate-host/".format(self.org1.subdomain), **headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_validate_invalid_hostname(self):
        headers = {
            'HTTP_ORIGIN': "http://doesnotexist.example.com/"
        }
        response = self.client.get('/api/v1/organization/validate-host/', **headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_with_correct_credentials(self):
        user = self.org1_admin_user
        user.set_password('password')
        user.save()
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        data = {
            'email': user.email,
            'password': 'password'
        }
        response = self.client.post('/api/v1/organization/login/', data, **headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        token = Token.objects.get(user=user)
        self.assertEqual(response.data['token'], token.key)

    def test_login_with_incorrect_credentials(self):
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        data = {
            'email': self.org1_admin_user.email,
            'password': 'random-string-123'
        }
        response = self.client.post('/api/v1/organization/login/', data, **headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_with_invalid_hostname(self):
        headers = {
            'HTTP_ORIGIN': "http://doesnotexist.example.com/".format(self.org1.subdomain)
        }
        data = {
            'email': self.org1_admin_user.email,
            'password': 'random-string-123'
        }
        response = self.client.post('/api/v1/organization/login/', data, **headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_anonymous_user_read_access_over_any_organization(self):
        client = APIClient()
        response = client.get('/api/v1/organization/{}/'.format(self.org1.id))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_anonymous_user_write_access_over_own_organization(self):
        client = APIClient()
        data = {
            'title': "This is a dummy title"
        }
        response = client.get('/api/v1/organization/{}/'.format(self.org1.id))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_standard_user_read_access_over_own_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_standard_user)
        response = client.get('/api/v1/organization/{}/'.format(self.org1.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_standard_user_write_access_over_own_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_standard_user)
        data = {
            'title': "This is a dummy title"
        }
        response = client.patch('/api/v1/organization/{}/'.format(self.org1.id), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_standard_user_read_over_other_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_standard_user)
        response = client.get('/api/v1/organization/{}/'.format(self.org2.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_standard_user_write_access_over_other_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_standard_user)
        data = {
            'title': "This is a dummy title"
        }
        response = client.patch('/api/v1/organization/{}/'.format(self.org2.id), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_user_read_access_over_own_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_admin_user)
        response = client.get('/api/v1/organization/{}/'.format(self.org1.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_user_write_access_over_own_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_admin_user)
        data = {
            'title': "This is a dummy title"
        }
        response = client.patch('/api/v1/organization/{}/'.format(self.org1.id), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_user_read_over_other_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_admin_user)
        response = client.get('/api/v1/organization/{}/'.format(self.org2.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_user_write_access_over_other_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_admin_user)
        data = {
            'title': "This is a dummy title"
        }
        response = client.patch('/api/v1/organization/{}/'.format(self.org2.id), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_standard_user_write_over_own_profile(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_standard_user)
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        data = {
            'first_name': "Dummy"
        }
        response = client.patch('/api/v1/user/{}/'.format(self.org1_standard_user.id), data, **headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_standard_user_write_over_other_user_in_same_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_standard_user)
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        data = {
            'first_name': "Dummy"
        }
        response = client.patch('/api/v1/user/{}/'.format(self.org1_standard_user1.id), data, **headers)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_standard_user_write_over_other_user_in_other_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_standard_user)
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        data = {
            'first_name': "Dummy"
        }
        response = client.patch('/api/v1/user/{}/'.format(self.org2_standard_user.id), data, **headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_admin_user_write_over_own_profile(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_admin_user)
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        data = {
            'first_name': "Dummy"
        }
        response = client.patch('/api/v1/user/{}/'.format(self.org1_admin_user.id), data, **headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_user_write_over_other_user_in_same_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_admin_user)
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        data = {
            'first_name': "Dummy",
            'org_user_profile': {
                'role': models.OrganizationUserProfile.STANDARD_ROLE
            }
        }
        response = client.patch('/api/v1/user/{}/'.format(self.org1_standard_user1.id), data, format='json', **headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_user_write_over_other_user_in_other_organization(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_admin_user)
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        data = {
            'first_name': "Dummy"
        }
        response = client.patch('/api/v1/user/{}/'.format(self.org2_standard_user.id), data, **headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_admin_user_create_own_organization_user(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_admin_user)
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        data = {
            'first_name': "Dummy",
            'last_name': "Dummy",
            'email': "dummyemail@dummyemail.com",
            'org_user_profile': {
                'role': models.OrganizationUserProfile.STANDARD_ROLE,
            }
        }
        response = client.post('/api/v1/user/', data, format='json', **headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_standard_user_create_own_organization_user(self):
        client = APIClient()
        client.force_authenticate(user=self.org1_standard_user)
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        data = {
            'first_name': "Dummy",
            'last_name': "Dummy",
            'email': "dummyemail1@dummyemail.com",
            'org_user_profile': {
                'role': models.OrganizationUserProfile.STANDARD_ROLE,
            }
        }
        response = client.post('/api/v1/user/', data, format='json', **headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_anonymous_user_read_over_any_organization_user(self):
        client = APIClient()
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        response = client.get('/api/v1/user/', **headers)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_anonymous_user_write_over_any_organization_user(self):
        client = APIClient()
        headers = {
            'HTTP_ORIGIN': "http://{}.example.com/".format(self.org1.subdomain)
        }
        data = {
            'title': "This is a dummy title"
        }
        response = client.post('/api/v1/user/', data, **headers)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
