from django.test import TestCase
from model_mommy import mommy
from rest_framework.test import APIClient


class OrganizationUserModelTest(TestCase):

    def setUp(self):
        self.client = APIClient()

        self.org1 = mommy.make_recipe('organization.test_org')
        self.org2 = mommy.make_recipe('organization.test_org')

        self.org1_admin_user = mommy.make_recipe('organization.org_user')
        self.org1_standard_user = mommy.make_recipe('organization.org_user')
        self.org2_admin_user = mommy.make_recipe('organization.org_user')
        self.org1_standard_user = mommy.make_recipe('organization.org_user')

        self.org1_admin_user_profile = mommy.make_recipe(
            'organization.org_admin_user_profile',
            organization=self.org1,
            user=self.org1_admin_user
        )
        self.org2_user_profile = mommy.make_recipe(
            'organization.org_admin_user_profile',
            organization=self.org2,
            user=self.org2_admin_user
        )

    def test_admin_user_on_own_organization(self):
        self.assertTrue(self.org1_admin_user_profile.is_organization_admin, True)
