class OrganizationKwargMissingError(Exception):
    def __init__(self, message="Organization kwarg is missing"):
        self.message = message

    def __str__(self):
        return self.message


