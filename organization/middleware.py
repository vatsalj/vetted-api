from urlparse import urlparse
from django.utils.deprecation import MiddlewareMixin
from django.utils.functional import SimpleLazyObject

from organization import models


def get_organization(request):
    def _get_organization_from_origin(origin):
        organization = None
        if origin:
            origin_parts = urlparse(origin).netloc.split('.')
            subdomain = origin_parts[0]
            try:
                organization = models.Organization.objects.get(subdomain=subdomain)
            except models.Organization.DoesNotExist:
                pass
        return organization

    if not hasattr(request, '_cached_organization'):
        request._cached_organization = _get_organization_from_origin(request.META.get('HTTP_ORIGIN'))
    return request._cached_organization


class OrganizationMiddleware(MiddlewareMixin):
    def process_request(self, request):
        request.organization = SimpleLazyObject(lambda: get_organization(request))
