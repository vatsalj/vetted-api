# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class OrganizationConfig(AppConfig):
    name = 'organization'

    def ready(self):
        import organization.signals.handlers  # noqa
