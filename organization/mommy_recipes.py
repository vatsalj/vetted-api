from model_mommy.recipe import Recipe, seq, foreign_key
from django.contrib.auth import get_user_model

from organization import models

UserModel = get_user_model()

test_org = Recipe(
    models.Organization,
    title=seq("Test Organization"),
    subdomain=seq("test")
)

org_user = Recipe(
    models.User,
    email=seq('a@b.com'),
    user_type=models.User.ORGANIZATION_USER,
)

org_standard_user_profile = Recipe(
    models.OrganizationUserProfile,
    organization=foreign_key(test_org),
    user=foreign_key(org_user),
    role=models.OrganizationUserProfile.STANDARD_ROLE
)

org_admin_user_profile = Recipe(
    models.OrganizationUserProfile,
    organization=foreign_key(test_org),
    user=foreign_key(org_user),
    role=models.OrganizationUserProfile.ADMIN_ROLE
)
