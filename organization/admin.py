# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.contrib import admin
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.utils.safestring import mark_safe

from organization import models


class OrganizationAdmin(admin.ModelAdmin):
    readonly_fields = ['invite_user_link']

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return 'invite_user_link',
        else:
            return ()

    def invite_user_link(self, obj):
        return mark_safe('<a href="../invite-user/">Invite user</a>')

    def get_urls(self):
        urls = super(OrganizationAdmin, self).get_urls()
        org_user_invitation_url = [
            url(r'^(?P<id>\d+)/invite-user/$', self.admin_site.admin_view(self.invite_user_view)),
        ]
        return org_user_invitation_url + urls

    def invite_user_view(self, request, **kwargs):
        organization = get_object_or_404(models.Organization, id=kwargs['id'])
        context = dict(
            self.admin_site.each_context(request),
            organization=organization,
        )
        return TemplateResponse(request, "admin_invite_org_user.html", context)

    def save_model(self, request, obj, form, change):
        super(OrganizationAdmin, self).save_model(request, obj, form, change)
        messages.info(
            request,
            mark_safe("Invite users to this organization from <a href=\"./{}/invite-user/\">here</a>".format(obj.id))
        )


admin.site.register(models.Organization, OrganizationAdmin)
admin.site.register(models.User)
admin.site.register(models.OrganizationUserProfile)
