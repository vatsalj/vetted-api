from django.contrib.auth import get_user_model
from rest_framework import authentication
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from organization import models
from organization.api import permissions as org_permissions
from organization.api import serializers as org_serializers
from organization.signals import inactive_user_created

from utils.rest_framework.permissions import IsSuperUser

UserModel = get_user_model()


class OrganizationViewSet(ModelViewSet):
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = (org_permissions.OrganizationCRUDPermission,)
    queryset = models.Organization.objects.all()
    serializer_class = org_serializers.OrganizationSerializer

    @action(methods=['post'], detail=False, url_path='login', permission_classes=[AllowAny])
    def login(self, request):
        serializer = org_serializers.UserLoginSerializer(
            data=request.data,
            context={'organization': request.organization}
        )
        if serializer.is_valid():
            user = serializer.validated_data['user']
            token, _ = Token.objects.get_or_create(user=user)
            return Response({
                'token': token.key,
                'user': serializer.validated_data['user_details']
            })
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['get'], detail=False, url_path='validate-host', permission_classes=[AllowAny])
    def validate_host(self, request):
        """
        Check if organization path is valid
        :return: boolean
        """
        valid_host = False
        try:
            _ = self.request.organization.id
            valid_host = True
        except AttributeError:
            pass
        return Response({
            'valid': valid_host
        }, status=[400, 200][valid_host])


class UserViewSet(ModelViewSet):
    authentication_classes = [authentication.TokenAuthentication, authentication.SessionAuthentication]
    permission_classes = (org_permissions.UserCRUDPermission,)
    serializer_class = org_serializers.UserSerializer

    def get_serializer_class(self):
        if self.request.user.is_superuser:
            return org_serializers.UserAdminSerializer
        return org_serializers.UserSerializer

    def get_serializer_context(self):
        context = super(UserViewSet, self).get_serializer_context()
        context['organization'] = self.request.organization
        return context

    def get_queryset(self):
        return models.User.org_users.filter(organization=self.request.organization)

    def perform_create(self, serializer):
        invitee = serializer.save()
        invitor = self.request.user
        inactive_user_created.send(sender=self, invitee=invitee, invitor=invitor)
