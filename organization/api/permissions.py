from django.contrib.auth import get_user_model
from rest_framework import permissions


class OrganizationCRUDPermission(permissions.IsAuthenticated):
    def has_object_permission(self, request, view, obj):
        """
        Allow organization's user read access over own organization only
        :param request:
        :param view:
        :param obj:
        :return:
        """
        return all([
            request.user.user_type == get_user_model().ORGANIZATION_USER,  # org user
            request.method in permissions.SAFE_METHODS,  # read access
            request.user.org_user_profile.organization == obj  # over own organization
        ])


class UserCRUDPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if request.user.is_authenticated:
            if request.user.user_type == get_user_model().ORGANIZATION_USER:
                return True
        return False

    def has_object_permission(self, request, view, obj):
        """
        Allow organization admin to add users
        Allow organization standard users to edit themselves
        :param request:
        :param view:
        :param obj:
        :return:
        """
        if request.user.is_superuser:
            return True
        if request.user.user_type == get_user_model().ORGANIZATION_USER:
            if request.user.org_user_profile.is_organization_admin:
                return True
            else:
                return request.user == obj
        return False
