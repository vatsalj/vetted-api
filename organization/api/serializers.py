from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.shortcuts import get_object_or_404

from organization import models

UserModel = get_user_model()


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Organization
        fields = ('id', 'title', 'subdomain',)


class OrganizationUserProfileSerializer(serializers.ModelSerializer):
    organization = OrganizationSerializer(allow_null=True, required=False)
    role_display = serializers.SerializerMethodField()

    class Meta:
        model = models.OrganizationUserProfile
        fields = ('role', 'organization', 'role_display',)

    def get_role_display(self, obj):
        return obj.get_role_display()


class UserSerializer(serializers.ModelSerializer):
    org_user_profile = OrganizationUserProfileSerializer()
    email = serializers.EmailField(required=True, allow_blank=False)

    class Meta:
        model = get_user_model()
        fields = ('id', 'first_name', 'last_name', 'email', 'org_user_profile',)

    def validate(self, attrs):
        email = attrs.get('email')
        if self.instance:
            if UserModel.org_users.filter(
                email=email,
                organization=self.context.get('organization')
            ).exclude(id=self.instance.id).exists():
                raise serializers.ValidationError({'email': "This email is already in the organization"})
        else:
            if UserModel.org_users.filter(
                email=email,
                organization=self.context.get('organization')
            ).exists():
                raise serializers.ValidationError({'email': "This user already exists"})
        return attrs

    def create(self, validated_data):
        organization = self.context.get('organization')
        user = get_user_model().org_users.create(organization=organization, **validated_data)
        return user

    def update(self, instance, validated_data):
        # Nested fields are not supported by ModelSerializer, so pop that & save individually
        org_user_profile_data = validated_data.pop('org_user_profile', None)
        user = super(UserSerializer, self).update(instance, validated_data)

        if org_user_profile_data:
            org_user_profile = user.org_user_profile
            org_user_profile_serializer = OrganizationUserProfileSerializer(
                instance=org_user_profile,
                data=org_user_profile_data
            )
            if org_user_profile_serializer.is_valid():
                org_user_profile_serializer.save()
        return user


class UserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(allow_blank=False)
    password = serializers.CharField(allow_blank=False)

    def validate(self, attrs):
        organization = self.context.get('organization')
        if not organization:
            raise serializers.ValidationError("Invalid organization subdomain - please check the URL")

        username = UserModel.get_org_username(attrs.get('email'), organization.id)
        password = attrs.get('password')
        user = None
        try:
            user = UserModel.org_users.get(username=username, organization=organization)
        except UserModel.DoesNotExist:
            UserModel().set_password(password)
        if user and user.is_active:
            if user.check_password(password):
                attrs['user'] = user
                attrs['user_details'] = UserSerializer(instance=user).data
                return attrs
            else:
                raise serializers.ValidationError("Invalid authentication credentials provided")
        else:
            raise serializers.ValidationError("User does not exist or is inactive")


class OrganizationAdminField(serializers.Field):
    def to_internal_value(self, data):
        return get_object_or_404(models.Organization, id=data)

    def to_representation(self, value):
        return "Organization ID: {}".format(value)


class UserAdminSerializer(serializers.ModelSerializer):
    org_user_profile = OrganizationUserProfileSerializer()
    email = serializers.EmailField(required=True, allow_blank=False)
    organization = OrganizationAdminField(write_only=True)

    class Meta:
        model = get_user_model()
        fields = ('id', 'first_name', 'last_name', 'email', 'org_user_profile', 'organization',)

    def validate(self, attrs):
        email = attrs.get('email')
        organization = attrs.get('organization')
        if not self.instance:
            if UserModel.org_users.filter(
                    email=email,
                    organization=organization,
            ).exists():
                raise serializers.ValidationError({'email': "This user already exists"})
        return attrs

    def create(self, validated_data):
        return get_user_model().org_users.create(**validated_data)
