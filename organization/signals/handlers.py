from invite.signals import invitation_accepted, invitation_rejected
from django.dispatch import receiver
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save

UserModel = get_user_model()


@receiver(invitation_accepted)
def activate_user(sender, user, password, **kwargs):
    user.is_active = True
    user.set_password(password)
    user.save()
