from django.dispatch import Signal


inactive_user_created = Signal(providing_args=['invitee', 'invitor', ],)
