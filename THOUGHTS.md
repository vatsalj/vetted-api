# Final email thoughts
## Multi-tenancy
The project is made multi-tenant with the use of a foreign key
for all organizations. This has the obvious disadvantage of
coupling of tenant isolation within the application itself ([see this][1]). If we
need to better separation of tenant data, we can choose to keep the
Postgres schemas / databases separate for all organizations.
To achieve independent scalability, docker will work just fine.

## User management
I've chosen to keep a single user model for everything at the moment.
If separate user models are required, [a custom authentication back-end
can be written which can support both user types][2]
If a complex user management system is required, creating a RBAC service
can be created as well.

## Teams
Need clarification

## Code style
I've chosen to keep 120 chars line length rather than the standard 80 chars.


[1]: (http://books.agiliq.com/projects/django-multi-tenant/en/latest/introduction.html#the-various-approached-to-multi-tenancy)
[2]: (https://churchman.nl/2017/09/28/creating-multiple-custom-user-types-through-inheritance-in-django/)