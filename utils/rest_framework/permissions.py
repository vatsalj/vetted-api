from rest_framework import permissions


class IsSuperUser(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        return request.user.is_superuser
